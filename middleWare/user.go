package middleWare

import (
	"gallery-api/models"
	"strings"

	"github.com/gin-gonic/gin"
)

func RequireUser(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		header = strings.TrimSpace(header)
		min := len("Bearer ")
		if len(header) <= min {
			c.Status(401)
			c.Abort()
			return
		}

		token := header[min:]

		user, err := us.GetByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}
		c.Set("user", user)

		// userLogin := c.Value("user").(User) //วิธีเรียกใช้ value ใน context
		// iT same same redux
	}
}
