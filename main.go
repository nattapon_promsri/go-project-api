package main

import (
	"gallery-api/config"
	"gallery-api/handlers"
	"gallery-api/hash"
	"gallery-api/middleWare"
	"gallery-api/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// const hmacKey = "nat_eiei"

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)

	// gorm.Open(
	// "mysql",
	// "root:password@tcp(127.0.0.1:3306)/gallerydb?parseTime=true",
	// )

	if err != nil {
		log.Fatal(err)
	} //ดักerror

	defer db.Close()
	db.LogMode(true) // dev only

	if err := db.AutoMigrate(&models.Gallery{}, &models.User{}, &models.Image{}).Error; err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey)

	gs := models.NewGalleryService(db)
	gh := handlers.NewGalleryHandler(gs)

	us := models.NewUserService(db, hmac)
	uh := handlers.NewUserHandler(us)

	ims := models.NewImageService(db)
	imh := handlers.NewImageHandler(gs, ims)

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	config.AllowHeaders = []string{"Content-Type", "Authorization"}
	r.Use(cors.New(config))

	r.Static("/upload", "./upload")

	r.POST("/signup/", uh.SignUp)
	r.POST("/login", uh.LogIn)

	r.GET("/galleries", gh.ListGallery) //pubilc

	r.GET("/galleries/:id", gh.GetOneGallery)
	r.GET("/galleries/:id/images", gh.GetImagesFromGallery)

	auth := r.Group("/auth")
	auth.Use(middleWare.RequireUser(us))
	{
		auth.POST("/galleries", gh.CreateGallery)
		auth.GET("/galleries", gh.MyGalleryList) // private

		auth.DELETE("/galleries/:id", gh.DeleteGallery)
		auth.PATCH("/galleries/:id/publishes", gh.UpdatePublishing)
		auth.PATCH("/galleries/:id/names", gh.UpdateName)

		auth.POST("/galleries/:id/images", imh.CreateImage)
		auth.GET("/galleries/:id/images", imh.ListGalleryImages)
		auth.DELETE("/images/:id", imh.DeleteImage)
	}

	r.Run()
}

// auth.GET("/session", func(c *gin.Context) {
// 	user ,ok := c.Value("user").(models.User)
// 	if !ok {
// 		c.JSON(401, gin.H{
// 			"message" : "invalid token",
// 		})
// 		return
// 	}
// 	c.JSON(200 , user)
// }

// auth.POST("/logout", uh.Logout)
// admin := auth.Group("/admin")
// {
// auth.POST("/galleries", gh.CreateGallery)
// auth.GET("/gallerie", gh.GetGalleryByID) // private
// admin.GET("/galleries/:id", gh.GetOne)

// auth.POST(("/image" , ih.Image))
// auth.DELETE("/galleries" , gh.DeleteGallery)
// auth.PUT("/galleries" , gh.UpdateGallery)
// auth.POST("/upload" , ih.UpdateImage)
// }
