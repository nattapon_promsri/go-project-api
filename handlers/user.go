package handlers

import (
	"gallery-api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us models.UserService
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}

type UserHandlerReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}


func (uh *UserHandler) SignUp(c *gin.Context) {

	data := new(UserHandlerReq)

	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user.Email = data.Email
	user.Password = data.Password

	if err := uh.us.SignUp(user); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(201, gin.H{
		"token": user.Token,
	})

}

type UserLoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (uh *UserHandler) LogIn(c *gin.Context) {

	userLoginReq := new(UserLoginReq)

	if err := c.BindJSON(userLoginReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	userLogin := new(models.User)
	userLogin.Email = userLoginReq.Email
	userLogin.Password = userLoginReq.Password

	token, err := uh.us.SignIn(userLogin)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

// func (uh *UserHandler) LogOut(c *gin.Context) {
// 	user := context.User(c)
// 	if user == nil {
// 		Error(c, 401, errors.New("invalid token"))
// 		return
// 	}
// 	err := uh.us.LogOut(user)
// 	if err != nil {
// 		Error(c, 500, err)
// 		return
// 	}
// 	c.Status(204)
// }
