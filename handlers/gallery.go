package handlers

import (
	"gallery-api/context"
	"gallery-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type GalleryHandler struct {
	gs models.GalleryService
}

type CreateReq struct {
	Name     string `json:"name"`
	IsPublic bool   `json:"is_public"`
}

func NewGalleryHandler(gs models.GalleryService) *GalleryHandler {
	return &GalleryHandler{gs}
}

type GalleryRes struct {
	ID       uint       `json:"id"`
	Name     string     `json:"name"`
	IsPublic bool       `json:"is_public"`
	Images   []ImageRes `json:"images"`
}

type CreateRes struct {
	GalleryRes
}

//////////////////////////////////////// function Gallery ////////////////////////////////////////

func (gh *GalleryHandler) ListGallery(c *gin.Context) {
	galliers, err := gh.gs.List()
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.JSON(200, galliers)
}

func (gh *GalleryHandler) GetImagesFromGallery(c *gin.Context) {

	// รับไอดีของ gallery มา
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	data, err := gh.gs.GetImagesByGalleryID(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.JSON(200, data)

	// ในตารา อิมเมจ หา ภาพที่ไอดี = ที่รับเข้ามา
	// คืน list รูปที่หาเจอ

}

func (gh *GalleryHandler) CreateGallery(c *gin.Context) {

	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}

	dataReq := new(CreateReq)
	if err := c.BindJSON(dataReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	gallery := new(models.Gallery)
	gallery.Name = dataReq.Name
	gallery.IsPublic = dataReq.IsPublic
	gallery.UserID = user.ID
	if err := gh.gs.Create(gallery); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	res := new(CreateRes)
	res.ID = gallery.ID
	res.Name = gallery.Name
	res.IsPublic = gallery.IsPublic

	c.JSON(201, res)
}

func (gh *GalleryHandler) MyGalleryList(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := gh.gs.ListByUserID(user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	galleries := []GalleryRes{}
	for _, d := range data {
		galleries = append(galleries, GalleryRes{
			ID:       d.ID,
			Name:     d.Name,
			IsPublic: d.IsPublic,
		})
	}
	c.JSON(200, galleries)
}

func (gh *GalleryHandler) GetOneGallery(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	data, err := gh.gs.GetByID(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.JSON(200, GalleryRes{
		ID:       data.ID,
		Name:     data.Name,
		IsPublic: data.IsPublic,
	})
}

func (gh *GalleryHandler) DeleteGallery(c *gin.Context) {

	ID := c.Param("id")
	id, err := strconv.Atoi(ID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	if err := gh.gs.DeleteGallery(uint(id)); err != nil {

		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(204)
}

////////////////////////////////////////////////////////////////////// after add Gallery ///////////////////////////////////

type UpdateStatusReq struct {
	IsPublic bool `json:"is_public"`
}

func (gh *GalleryHandler) UpdatePublishing(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	err = gh.gs.UpdateGalleryPublishing(uint(id), req.IsPublic)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}

type UpdateNameReq struct {
	Name string `json:"name"`
}

func (gh *GalleryHandler) UpdateName(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	req := new(UpdateNameReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	err = gh.gs.UpdateGalleryName(uint(id), req.Name)
	if err != nil {
		Error(c, 500, err)
		return
	}
	c.Status(204)
}