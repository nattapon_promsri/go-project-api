package models

import (
	"gallery-api/hash"
	// "crypto/sha256"

	// "fmt"
	"gallery-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const cost = 4

type User struct {
	gorm.Model
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Token    string `gorm:"unique_index" json:"token"`
}

type UserService interface {
	SignUp(temp *User) error
	SignIn(user *User) (string, error)
	GetByToken(token string) (user *User, err error)
	// Logout(user *User) error
}

type UserGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	return &UserGorm{db, hmac}
}

func (ug *UserGorm) SignUp(temp *User) error {

	user := new(User)
	user.Email = temp.Email
	user.Password = temp.Password

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}

	tokenHash := ug.hmac.Hash(token)

	user.Token = tokenHash
	temp.Token = token

	return ug.db.Create(user).Error

	// userSave := new(User)

	// userSave.Email = user.Email
	// userSave.Password = user.Password

	// hash, err := bcrypt.GenerateFromPassword([]byte(userSave.Password), cost)
	// if err != nil {
	// 	return err
	// }

	// userSave.Password = string(hash)
	// token, err := rand.GetToken()
	// if err != nil {
	// 	return err
	// }
	// user.Token = token

	// ug.hmac.Write([]byte(token))
	// newToken := ug.hmac.Sum(nil)
	// newTokenStr := base64.RawURLEncoding.EncodeToString(newToken)

	// userSave.Token = newTokenStr

	// if err := ug.db.Create(userSave).Error; err != nil {
	// 	return err
	// }

	// return nil

}

func (ug *UserGorm) SignIn(user *User) (string, error) {

	found := new(User)
	err := ug.db.Where("email = ?", user.Email).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	tokenHash := ug.hmac.Hash(token)

	err = ug.db.Model(&User{}).
		Where("id = ?", found.ID).
		Update("token", tokenHash).Error
	if err != nil {
		return "", err
	}
	return token, nil

	// found := new(User)

	// err := ug.db.Where("email = ?", user.Email).First(&found).Error
	// if err != nil {
	// 	return "", err
	// }
	// err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	// if err != nil {
	// 	return "", err
	// }

	// token, err := rand.GetToken()
	// if err != nil {
	// 	return "", err
	// }

	// ug.hmac.Write([]byte(token))
	// newToken := ug.hmac.Sum(nil)
	// newTokenStr := base64.RawURLEncoding.EncodeToString(newToken)

	// err = ug.db.Model(&User{}).Where("id = ?", found.ID).Update("Token", newTokenStr).Error
	// if err != nil {
	// 	return "", err
	// }
	// return token, nil
}

func (ug *UserGorm) GetByToken(token string) (*User, error) {

	tokenHash := ug.hmac.Hash(token)
	user := new(User)
	err := ug.db.Where("token = ?", tokenHash).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil

	// ug.hmac.Write([]byte(token))
	// newToken := ug.hmac.Sum(nil)
	// newTokenStr := base64.RawURLEncoding.EncodeToString(newToken)

	// if err = ug.db.Where("token = ?", newTokenStr).First(&user).Error; err != nil {
	// 	return user, err
	// }
	// return user, err

}

// func (ug *UserGorm) LogOut (user *User) error{
// 	return ug.db.Model(user).
// 		Where("id = ?", user.ID).
// 		Update("token", "").Error
// }
